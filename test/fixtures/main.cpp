/**
Codigo escrito por christian portilla pauca

Email : xhrist14n@gmail.com

Generador de data dump para pruebas de smartsensor api.

*/

#include <iostream>
#include <sstream>
#include <fstream>

#include <cassert>

#include <cstring>
#include <cstdio>
#include <cstdlib>

#include <vector>

#include <ctime>




using namespace std;

std::string sensors(){
    std::string  result="";
    stringstream ss;

    vector<string> *sensors=new vector<string>();

    sensors->push_back("temperature");
    sensors->push_back("humidity");
    sensors->push_back("sound");
    sensors->push_back("flowmeter");
    sensors->push_back("light");
    sensors->push_back("nitrogen_dioxide");
    sensors->push_back("carbon_monoxide");
    sensors->push_back("ultraviolet");

    vector<string> *unities=new vector<string>();

    unities->push_back("centigrades");
    unities->push_back("percent");
    unities->push_back("db");
    unities->push_back("m3/h");
    unities->push_back("lux");
    unities->push_back("pbm");
    unities->push_back("ppm");
    unities->push_back("lux");

    int cont=0;
    string comma="";
    for(unsigned int i=0;i<sensors->size();i++){
        if(cont>0){comma=",";}
        cont++;
        ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.Sensor\",\n         \"pk\":\""<<(int)(i+1)<<"\","
            <<"\n         \"fields\":{"
            <<"\n            \"id\":\""<<(int)(i+1)<<"\","
            <<"\n            \"name\":\""<<sensors->at(i)<<"\","
            <<"\n            \"unity\":\""<<unities->at(i)<<"\","
            <<"\n            \"minimum_range\":\"0\","
            <<"\n            \"maximum_range\":\"100\""
            <<"\n         }"
            <<"\n      }"<<endl;
    }
    result=ss.str();
    return result;
}

std::string devices(){
    std::string  result="";
    stringstream ss;
    string comma=",";

    vector<string> *users=new vector<string>();

    users->push_back("juan");
    users->push_back("marcos");

    vector<string> *devices=new vector<string>();

    devices->push_back("166d77ac1b46a1ec38aa35ab7e628ab5");


    vector<string> *sensors=new vector<string>();

    sensors->push_back("temperature");
    sensors->push_back("humidity");
    sensors->push_back("sound");
    sensors->push_back("flowmeter");
    sensors->push_back("light");
    sensors->push_back("nitrogen_dioxide");
    sensors->push_back("carbon_monoxide");
    sensors->push_back("ultraviolet");

    vector<string> *lats=new vector<string>();

    lats->push_back("-16.3861304");


    vector<string> *lngs=new vector<string>();

    lngs->push_back("-71.5528597");


    int cont=1;
    int cont_dev=1;
    for(unsigned int u=0;u<users->size();u++){
        ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.Login\",\n         \"pk\":\""<<(int)(u+1)<<"\","
            <<"\n         \"fields\":{"
            <<"\n            \"id\":\""<<(int)(u+1)<<"\","
            <<"\n            \"name\":\""<<users->at(u)<<"\","
            <<"\n            \"lastname\":\""<<users->at(u)<<"\","
            <<"\n            \"nickname\":\""<<users->at(u)<<"\","
            <<"\n            \"password\":\""<<users->at(u)<<"\","
            <<"\n            \"email\":\""<<users->at(u)<<"@gmail.com\","
            <<"\n            \"token\":\""<<users->at(u )<<"\""
            <<"\n         }"
            <<"\n      }"<<endl;
        for(unsigned int i=0;i<devices->size();i++){
            ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.Device\",\n         \"pk\":\""<<(int)(i+1+u)<<"\","
                <<"\n         \"fields\":{"
                <<"\n            \"id\":\""<<cont_dev<<"\","
                <<"\n            \"token\":\""<<devices->at(i)<<"\","
                <<"\n            \"latitude\":\""<<lats->at(i)<<"\","
                <<"\n            \"longitude\":\""<<lngs->at(i)<<"\","
                <<"\n            \"name\":\""<<devices->at(i)<<"\","
                <<"\n            \"dnsname\":\"device"<<(int)(i+1)<<".devices.com\","
                <<"\n            \"port\":\"8080\","
                <<"\n            \"city\":\"arequipa\","
                <<"\n            \"country\":\"peru\","
                <<"\n            \"state\":\"10100101\","
                <<"\n            \"type_network\":\"wireless\","
                <<"\n            \"sim_number\":\"0000000000000\""
                <<"\n         }"
                <<"\n      }"<<endl;
            ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.OwnerDevice\",\n         \"pk\":\""<<(int)(i+1+u)<<"\","
                <<"\n         \"fields\":{"
                <<"\n            \"id\":\""<<(int)(i+1+u)<<"\","
                <<"\n            \"device\":\""<<cont_dev<<"\","
                <<"\n            \"owner\":\""<<(int)(u+1)<<"\","
                <<"\n            \"deleted\":\"0\""
                <<"\n         }"
                <<"\n      }"<<endl;

            for(unsigned int j=0;j<sensors->size();j++){
                ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.Access\",\n         \"pk\":\""<<(int)(cont)<<"\","
                    <<"\n         \"fields\":{"
                    <<"\n            \"id\":\""<<(int)(cont)<<"\","
                    <<"\n            \"device\":\""<<cont_dev<<"\","
                    <<"\n            \"sensor\":\""<<(int)(j+1)<<"\","
                    <<"\n            \"public\":\"True\""
                    <<"\n         }"
                    <<"\n      }"<<endl;
                    cont++;
            }
            cont_dev++;
        }
    }
    cout<<"Numero devices: "<<cont_dev<<endl;
    result=ss.str();
    return result;
}

std::string histories(){
    std::string  result="";
    stringstream ss;
    string comma=",";

    vector<string> *users=new vector<string>();

    users->push_back("juan");
    users->push_back("marcos");

    vector<string> *devices=new vector<string>();

    devices->push_back("166d77ac1b46a1ec38aa35ab7e628ab5");


    vector<string> *sensors=new vector<string>();

    sensors->push_back("flowmeter");
    sensors->push_back("nitro-oxygen");
    sensors->push_back("humidity");
    sensors->push_back("light");
    sensors->push_back("mono-oxyde");
    sensors->push_back("sound");
    sensors->push_back("temperature");
    sensors->push_back("ultraviolet");

    int cont=1;
    int cont_dev=1;


    int random;

    srand(time(NULL));

    for(int d=1;d<2;d++){
        for(int h=0;h<2;h++){
            for(int m=0;m<20;m+=5){
                cont_dev=1;
                for(unsigned int u=0;u<users->size();u++){
                    for(unsigned int i=0;i<devices->size();i++){
                        for(unsigned int j=0;j<sensors->size();j++){

                            random=(int)(rand()*1678462308);
                            random=(int)(random)%100;

                            if(random<0){random=random*(-1);}
                            ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.History\",\n         \"pk\":\""<<(int)(cont)<<"\","
                                <<"\n         \"fields\":{"
                                <<"\n            \"id\":\""<<(int)(cont)<<"\","
                                <<"\n            \"device\":\""<<cont_dev<<"\","
                                <<"\n            \"sensor\":\""<<(int)(j+1)<<"\","
                                <<"\n            \"date\":\"2015-03-"<<d<<" "<<h<<":"<<m<<":05\","
                                <<"\n            \"value\":\""<<random<<"\""
                                <<"\n         }"
                                <<"\n      }"<<endl;
                                cont++;


                        }
                        cont_dev++;
                    }
                }
            }
        }
    }

    cout<<"Numero devices: "<<cont_dev<<endl;
    result=ss.str();
    return result;
}


std::string schedulers(){
    std::string  result="";
    stringstream ss;
    string comma=",";

    vector<string> *users=new vector<string>();

    users->push_back("juan");
    users->push_back("marcos");

    vector<string> *devices=new vector<string>();

    devices->push_back("166d77ac1b46a1ec38aa35ab7e628ab5");


    vector<string> *schedulers=new vector<string>();

    schedulers->push_back("primero");
    schedulers->push_back("segundo");
    schedulers->push_back("tercero");


    int cont=1;
    int cont_dev=1;


    int random;

    srand(time(NULL));

    for(int d=1;d<2;d++){
        for(int h=0;h<2;h++){
            for(int m=0;m<20;m+=5){
                cont_dev=1;
                for(unsigned int u=0;u<users->size();u++){
                    for(unsigned int i=0;i<devices->size();i++){
                        for(unsigned int j=0;j<schedulers->size();j++){

                            random=(int)(rand()*1678462308);
                            random=(int)(random)%2;

                            if(random<0){random=random*(-1);}
                            ss<<"   "<<"   "<<comma<<"\{\n         \"model\":\"model.Scheduler\",\n         \"pk\":\""<<(int)(cont)<<"\","
                                <<"\n         \"fields\":{"
                                <<"\n            \"id\":\""<<(int)(cont)<<"\","
                                <<"\n            \"device\":\""<<cont_dev<<"\","
                                <<"\n            \"name\":\""<<schedulers->at(j)<<"\","
                                <<"\n            \"time_begin\":\""<<h<<":"<<m<<":15\","
                                <<"\n            \"time_end\":\""<<((h+1)%12)<<":"<<m<<":30\","
                                <<"\n            \"date_begin\":\"2015-03-"<<d<<" "<<h<<":"<<m<<":05\","
                                <<"\n            \"date_end\":\"2015-04-"<<((d+4)%28)<<" "<<h<<":"<<m<<":05\","
                                <<"\n            \"frecuency\":\"week\","
                                <<"\n            \"rule\":\"1,2,3,4\","
                                <<"\n            \"enabled\":\"True\","
                                <<"\n            \"valve\":\""<<(int)(random+1)<<"\""
                                <<"\n         }"
                                <<"\n      }"<<endl;
                                cont++;


                        }
                        cont_dev++;
                    }
                }
            }
        }
    }
    cout<<"Numero devices: "<<cont_dev<<endl;

    result=ss.str();
    return result;
}

int main()
{

    stringstream ss;
    fstream fixtures("initial_data.json",ios::out);

    ss<<"["<<endl;
    ss<<sensors();
    ss<<devices();
    //ss<<histories();
    ss<<schedulers();
    ss<<"]"<<endl;

    //cout<<ss.str();

    fixtures<<ss.str();

    fixtures.close();

    return 0;
}