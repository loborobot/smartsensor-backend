/*
 * File:   main.cpp
 * Author: christian
 *
 * Created on 1 de Abril de 2015, 22:01
 */

#include <cstdlib>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>

using namespace std;

/*
 *
 */
int main(int argc, char** argv) {


    string command;
    double temperature=0;
    double humidity=0;
    double light=0;
    double ultra=0;
    double sound=0;
    double flow=0;
    double volume=0;
    double nitrogen=0;
    double carbon=0;
    double lat=0;
    double lon=0;
    srand(time(NULL));
    for(int i=0;i<5;i++){
        stringstream ss;
        temperature=(((int)(rand()*1000)/100))%300;
        if(temperature<0) temperature*=-1;
        temperature/=10;

        humidity=(((int)(rand()*1000)/100))%300;
        if(humidity<0) humidity*=-1;
        humidity/=10;

        light=(((int)(rand()*1000)/100))%300;
        if(light<0) light*=-1;
        light/=10;

        ultra=(((int)(rand()*1000)/100))%300;
        if(ultra<0) ultra*=-1;
        ultra/=10;

        sound=(((int)(rand()*1000)/100))%300;
        if(sound<0) sound*=-1;
        sound/=10;

        flow=(((int)(rand()*1000)/100))%300;
        if(flow<0) flow*=-1;
        flow/=10;

        volume=(((int)(rand()*1000)/100))%300;
        if(volume<0) volume*=-1;
        volume/=10;

        nitrogen=(((int)(rand()*1000)/100))%300;
        if(nitrogen<0) nitrogen*=-1;
        nitrogen/=10;

        carbon=(((int)(rand()*1000)/100))%300;
        if(carbon<0) carbon*=-1;
        carbon/=10;

        lat=(((int)(rand()*1000)/100))%114;
        if(lat<0) lat*=-1;
        lat/=10;

        lon=(((int)(rand()*1000)/100))%114;
        if(lon<0) lon*=-1;
        lon/=10;

        string url="http://127.0.0.1:9000/webapi/devices/";

        //cout<<temperature<<endl;
        ss<<"curl -v -i -H \"Content-type: application/json\" "
        <<" -H \"Accept: */*\" "
        //<<" -H \"Content-Length: 304\" "
        //<<" -H \"Connection: close \" "
        <<"-X POST http://192.168.1.201:8080/webapi/update/ -d '{";
        ss<<"\"device\": \"166d77ac1b46a1ec38aa35ab7e628ab5\","
            <<"\"pub_date\": \"2015-01-21 9:02:27.321\","
            <<"\"temp\": \""<<temperature<<"\","
            <<"\"hum\": \""<<humidity<<"\","
            <<"\"lig\": \""<<light<<"\","
            <<"\"uv\": \""<<ultra<<"\","
            <<"\"snd\": \""<<sound<<"\","
            <<"\"flm\": \""<<flow<<"\","
            <<"\"no2\": \""<<nitrogen<<"\","
            <<"\"co\": \""<<carbon<<"\""
            <<"}'  > out.html"<<endl;

        command=ss.str();
        //cout<<command<<endl;
        system(command.c_str());
    }

    //cout<<command<<endl;
    return 0;
}
