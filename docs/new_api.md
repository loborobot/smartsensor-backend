# for Devices

### POST     `/update/` ·
Update device status

```json
{ 
    "pub_date":             "datetime"
    "temperature":          "decimal(2, 2)",
    "humidity":             "decimal(2, 2)",
    "light":                "decimal(4, 2)",
    "ultra_violet":         "decimal(2, 1)",
    "sound":                "decimal(3, 1)",
    "flowmeter":            "decimal(2, 2)",
    "volume":               "decimal(3, 2)",
    "nitrogen_dioxide":     "decimal(2, 2)",
    "carbon_monoxide":      "decimal(4, 2)"
}
```

# for Clients

## User

### GET      `/user/profile/` ·
Retrieve current user info

```json
{ 
    "username":             "string",
    "first_name":           "string",
    "last_name":            "string",
    "email":                "string",
    "is_staff":             "boolean",
    "is_active":            "boolean",
    "date_joined":          "datetime"
}
```

### PATCH    `/user/profile/` ·
Update current user info

```json
{ 
    "first_name":           "string",
    "last_name":            "string",
    "email":                "string",
    "is_staff":             "boolean",
    "is_active":            "boolean"
}
```

## Devices

### GET      `/devices/`
List all devices of current user

```json
[
    {
        "gsm":                  "string",
        "status": {
            "pub_date":             "datetime"
            "temperature":          "decimal(2, 2)",
            "humidity":             "decimal(2, 2)",
            "light":                "decimal(4, 2)",
            "ultra_violet":         "decimal(2, 1)",
            "sound":                "decimal(3, 1)",
            "flowmeter":            "decimal(2, 2)",
            "volume":               "decimal(3, 2)",
            "nitrogen_dioxide":     "decimal(2, 2)",
            "carbon_monoxide":      "decimal(4, 2)"
        }
    },
    ...
]
```

### POST     `/devices/`
Create a new device

```json
{ 
    "gsm":                  "string"
}
```

### GET      `/devices/:id/`
Retrieve a device

```json
{ 
    "gsm":                  "string",
    "status": {
        "pub_date":             "datetime"
        "temperature":          "decimal(2, 2)",
        "humidity":             "decimal(2, 2)",
        "light":                "decimal(4, 2)",
        "ultra_violet":         "decimal(2, 1)",
        "sound":                "decimal(3, 1)",
        "flowmeter":            "decimal(2, 2)",
        "volume":               "decimal(3, 2)",
        "nitrogen_dioxide":     "decimal(2, 2)",
        "carbon_monoxide":      "decimal(4, 2)"
    }
}
```

### PUT      `/devices/:id/`
Update an existing device or create a new device

```json
{ 
    "gsm":                  "string"
}
```

### PATCH    `/devices/:id/`
Update an existing device

```json
{ 
    "gsm":                  "string"
}
```

### DELETE   `/devices/:id/`
Remove a device

### GET      `/devices/log/:id/`
List all status of a device

```json
[
    {
        "pub_date":             "datetime"
        "temperature":          "decimal(2, 2)",
        "humidity":             "decimal(2, 2)",
        "light":                "decimal(4, 2)",
        "ultra_violet":         "decimal(2, 1)",
        "sound":                "decimal(3, 1)",
        "flowmeter":            "decimal(2, 2)",
        "volume":               "decimal(3, 2)",
        "nitrogen_dioxide":     "decimal(2, 2)",
        "carbon_monoxide":      "decimal(4, 2)"
    },
    ...
]
```