# API RESTful

##   Devices

###  GET `/devices/`
List all devices

```json
[
    {
        "url":              "url",
        "gsm":              "string"
    },
    ...
]
```

###  POST `/devices/`
Create a new device

```json
{
    "gsm":                  "string"
}
```

### GET `/devices/:id/`
Retrieve the device :id

```json
{
    "url":                  "url",
    "gsm":                  "string"
}
```

### PUT `/devices/:id/`
Update or create the device :id 

```json
{
    "url":                  "url",
    "gsm":                  "string"
}
```

### PATCH `/devices/:id/`
Update the device :id 

```json
{
    "url":                  "url",
    "gsm":                  "string"
}
```

### DELETE `/devices/:id/`
Delete the device :id 

## Samples

### GET `/samples/`
List all samples

```json
[
    { 
        "url":                  "url",
        "device":               "url",
        "pub_date":             "date(iso 8601)",
        "temperature":          "decimal(2, 2)",
        "humidity":             "decimal(2, 2)",
        "light":                "decimal(4, 2)",
        "ultra_violet":         "decimal(2, 1)",
        "sound":                "decimal(3, 1)",
        "flowmeter":            "decimal(2, 2)",
        "volume":               "decimal(3, 2)",
        "nitrogen_dioxide":     "decimal(2, 2)",
        "carbon_monoxide":      "decimal(4, 2)"
    },
    ...
]
```

### POST `/samples/`
Create a new sample

```json
{ 
    "device":               "url",
    "temperature":          "decimal(2, 2)",
    "humidity":             "decimal(2, 2)",
    "light":                "decimal(4, 2)",
    "ultra_violet":         "decimal(2, 1)",
    "sound":                "decimal(3, 1)",
    "flowmeter":            "decimal(2, 2)",
    "volume":               "decimal(3, 2)",
    "nitrogen_dioxide":     "decimal(2, 2)",
    "carbon_monoxide":      "decimal(4, 2)"
}
```

### GET `/samples/:id/`
Retrieve the sample :id

```json
{
    "url":                  "url",
    "device":               "url",
    "pub_date":             "date(iso 8601)",
    "temperature":          "decimal(2, 2)",
    "humidity":             "decimal(2, 2)",
    "light":                "decimal(4, 2)",
    "ultra_violet":         "decimal(2, 1)",
    "sound":                "decimal(3, 1)",
    "flowmeter":            "decimal(2, 2)",
    "volume":               "decimal(3, 2)",
    "nitrogen_dioxide":     "decimal(2, 2)",
    "carbon_monoxide":      "decimal(4, 2)"
}
```

### PUT `/samples/:id/`
Update or create the sample :id

```json
{
    "url":                  "url",
    "device":               "url",
    "pub_date":             "date(iso 8601)",
    "temperature":          "decimal(2, 2)",
    "humidity":             "decimal(2, 2)",
    "light":                "decimal(4, 2)",
    "ultra_violet":         "decimal(2, 1)",
    "sound":                "decimal(3, 1)",
    "flowmeter":            "decimal(2, 2)",
    "volume":               "decimal(3, 2)",
    "nitrogen_dioxide":     "decimal(2, 2)",
    "carbon_monoxide":      "decimal(4, 2)"
}
```

### PATCH `/samples/:id/`
Update the sample :id

```json
{
    "url":                  "url",
    "device":               "url",
    "pub_date":             "date(iso 8601)",
    "temperature":          "decimal(2, 2)",
    "humidity":             "decimal(2, 2)",
    "light":                "decimal(4, 2)",
    "ultra_violet":         "decimal(2, 1)",
    "sound":                "decimal(3, 1)",
    "flowmeter":            "decimal(2, 2)",
    "volume":               "decimal(3, 2)",
    "nitrogen_dioxide":     "decimal(2, 2)",
    "carbon_monoxide":      "decimal(4, 2)"
}
```

### DELETE `/samples/:id/`
Delete the sample :id
