import requests
import threading
import json
import time
from random import randint

host = 'http://127.0.0.1:8000/api'
auth = ('fincyt', 'fincyt')


def uf(dig0, dig1):
    return '%s.%s' % (
        randint(int('1' * dig0), int('9' * dig0)),
        randint(int('1' * dig1), int('9' * dig1)))


class Device(threading.Thread):
    """
    Device simulator
    """

    def __init__(self, url, run_event):
        super(Device, self).__init__()
        self.url = url
        self.run_event = run_event

    def read_sensors(self):
        return {
            'url':              self.url,
            'temperature':      uf(2, 2),
            'humidity':         uf(2, 2),
            'light':            uf(4, 2),
            'ultra_violet':     uf(2, 1),
            'sound':            uf(3, 1),
            'flowmeter':        uf(2, 2),
            'volume':           uf(3, 2),
            'nitrogen_dioxide': uf(2, 2),
            'carbon_monoxide':  uf(4, 2),
        }

    def run(self):
        #while self.run_event.is_set():
        data = json.dumps(self.read_sensors())
        print data
        headers = {'content-type': 'application/json'}
        a = requests.post(host + '/samples/', data, auth=auth, headers=headers)
        print a.text


def create_devices(run_event):
    tlist = []
    devices = requests.get(host + '/devices', auth=auth).json()
    for k, device in enumerate(devices):
        device = Device(device['url'], run_event)
        device.start()
        device.join()
        tlist.append(device)
    return tlist


if __name__ == "__main__":
    run_event = threading.Event()
    run_event.set()

    threads = create_devices(run_event)

    try:
        while True:
            time.sleep(.1)
    except KeyboardInterrupt:
        run_event.clear()
        for t in threads:
            t.join()