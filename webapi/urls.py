from django.conf.urls import patterns, url
from django.conf.urls import url
from rest_framework import routers
from django.conf.urls import patterns, include, url
from rest_framework import authentication, permissions
from rest_framework.response import Response
from django.core import serializers

from webapi import views

from rest_framework import generics

from django.conf.urls import patterns, url



router = routers.SimpleRouter()
router.register(r'devices', views.DeviceViewSet)
router.register(r'^device', views.DeviceOneViewSet)
router.register(r'sensors', views.SensorViewSet)
router.register(r'histories', views.HistoryViewSet)
router.register(r'pagedhistories', views.PagedHistoryViewSet)
router.register(r'accesses', views.AccessViewSet)
router.register(r'schedulers', views.SchedulerViewSet)

#urlpatterns = router.urls
urlpatterns = patterns(
    'webapi.views',
    url(r'^$', views.APIRoot.as_view()),
    url(r'', include(router.urls)),
    url(r'^update', 'update'),
)

