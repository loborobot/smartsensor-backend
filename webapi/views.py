from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from rest_framework import generics, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
# Create your views here.
from django.conf.urls import url, include
from django.contrib.auth.models import User

from django.shortcuts import get_object_or_404, render

from rest_framework import mixins

from model.models import *

from rest_framework import routers, serializers, viewsets

import django_filters

from libs.http import Request

import logging

from django.core.paginator import Paginator
from rest_framework import filters
from rest_framework import generics

from django.views.decorators.csrf import csrf_exempt

#from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
#from api.serializers import PaginatedUserSerializer, UserKarmaSerializer
#from rest_framework import pagination
#from rest_framework_extensions.mixins import PaginateByMaxMixin

logger = logging.getLogger(__name__)


class APIRoot(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    @staticmethod
    def get(request):
        return Response(

            {
                'devices':      "http://"+request.META['HTTP_HOST']+'/webapi/devices/',
                'device':       "http://"+request.META['HTTP_HOST']+'/webapi/device/',
                'sensors':      "http://"+request.META['HTTP_HOST']+'/webapi/sensors/',
                'histories':    "http://"+request.META['HTTP_HOST']+'/webapi/histories/',
                'pagedhistories':  "http://"+request.META['HTTP_HOST']+'/webapi/pagedhistories/',
                'accesses':     "http://"+request.META['HTTP_HOST']+'/webapi/accesses/',
                'schedulers':   "http://"+request.META['HTTP_HOST']+'/webapi/schedulers/',
            }  
            
        )

 
class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ( 'id','token', 'name','dnsname','port','city','country','latitude','longitude','state')
        
class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    
    
class DeviceOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ( 'id','token','name', 'dnsname','port','city','country','latitude','longitude','state')
        
class DeviceOneViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()    
    serializer_class = DeviceOneSerializer    
    filter_fields = ('token')

            
            
class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = ('id', 'name', 'unity', 'minimum_range','maximum_range')
        
class SensorViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','value')
        
class HistoryViewSet(viewsets.ModelViewSet):
    queryset = History.objects.all()
    serializer_class = HistorySerializer
    
class PagedHistoryViewSet(viewsets.ModelViewSet):
    queryset = History.objects.all()
    serializer_class = HistorySerializer
    filter_fields = ('date','device','sensor')
#    paginate_by = 10
#    paginate_by_param = 'page_size'
#    max_paginate_by = 100    
    ##filter_backends = (filters.OrderingFilter,)
    ##ordering_fields = 'id'
    
#class PagedHistoryViewSet(viewsets.ModelViewSet):
    #queryset = History.objects.all()
#    serializer_class = HistorySerializer   
    

class HistoryOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','remote_port')
        
class HistoryOneViewSet(viewsets.ModelViewSet):
    serializer_class = HistorySerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return History.objects.filter(device=deviceOne) 
    
class HistorySensorOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','remote_port')
        
class HistorySensorOneViewSet(viewsets.ModelViewSet):
    serializer_class = HistorySerializer
    def get_queryset(self,token_query,sensor_id): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        sensorOne=Sensor.objects.get(pk=sensor_id)
        return History.objects.filter(device=deviceOne,sensor=sensorOne) 
    
class AccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Access.objects.all()
    serializer_class = AccessSerializer

class AccessOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessOneViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = AccessOneSerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return Access.objects.filter(device=deviceOne) 
    
class AccessSensorOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessSensorOneViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = AccessSerializer
    def get_queryset(self,token_query,sensor_id): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        sensorOne=Sensor.objects.get(pk=sensor_id)
        return Access.objects.filter(device=deviceOne,sensor=sensorOne) 
    
class SchedulerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scheduler
        fields = ('id', 'name','time_begin', 'time_end', 'date_begin','date_end','frecuency','rule','enabled','remote_port')
        
class SchedulerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Scheduler.objects.all()
    serializer_class = SchedulerSerializer

class SchedulerOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scheduler
        fields = ('id','name', 'time_begin', 'time_end', 'date_begin','date_end','frecuency','rule','enabled','remote_port')
        
class SchedulerOneViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = SchedulerOneSerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return Scheduler.objects.filter(device=deviceOne)
    
    
@csrf_exempt
def update(request):    
    if request.method == 'POST':
        data = json.loads(request.body)    
        token_query=data['id']
        date=data["tstamp"]
        device=Device.objects.all().filter(token=token_query)
        try:
            device=device[0]
        except:
            device = None
            pass
        newparams = ['tmp', 'hum', 'lig', 'uv', 'snd', 'flm', 'no2', 'co']
        oldparams = ['temperature','humidity','light', 'ultraviolet', 'sound','flowmeter','nitrogen_dioxide','carbon_monoxide']
        counts = [0,1,2,3,4,5,6,7]

        for count in counts:
            try:
                sensor = oldparams[count]
                sense = newparams[count]
                data[sensor] = data[sense]
            except:
                continue
        for s in oldparams:
            value = ''
            try:
                value = data[s]
                #print '\n%s \n '%value
            except:
                value = ''
            object = Sensor.objects.get(name=s)
            if device is not None:
                history = History()
                history.device=device
                history.sensor=object
                history.date = date
                history.value=value
                history.save()

        return HttpResponse("ok")

    if request.method == 'GET':
        return HttpResponse("error")
 