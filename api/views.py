from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from rest_framework import generics, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from .models import Device, Sample
from . import serializers
from .permissions import IsOwner, IsCurrentUser


class Root(APIView):
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def get(request):
        return Response({
            'update': reverse('device-update', request=request),
            'profile': reverse('user-profile', request=request),
            'devices': reverse('device-list', request=request),
        })


class AuthView(APIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        user = request.DATA['user']
        user = authenticate(username=user['username'], password=user['password'])
        login(request, user)
        return Response(serializers.UserSerializer(request.user).data)

    def delete(self, request, *args, **kwargs):
        logout(request)
        return Response({})


class DeviceUpdateStatus(generics.CreateAPIView):
    queryset = Sample.objects.all()
    serializer_class = serializers.DeviceStatusSerializer
    permission_classes = (permissions.AllowAny,)


class UserProfile(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = serializers.UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated, IsCurrentUser)

    def get_object(self, queryset=None):
        return self.request.user


#
# TODO: change device list to public/private
# create a new view to list public devices
# modify this view to list user public/private devices
#
class DeviceList(generics.ListCreateAPIView):
    serializer_class = serializers.DeviceSerializer
    permission_classes = (permissions.IsAdminUser, IsOwner)

    def pre_save(self, obj):
        obj.owner = self.request.user

    def get_queryset(self):
        return Device.objects.filter(owner=self.request.user)


class DeviceDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.DeviceSerializer
    permission_classes = (permissions.IsAdminUser, IsOwner)

    def get_queryset(self):
        return Device.objects.all()
