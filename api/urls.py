from django.conf.urls import patterns, url
from api import views

urlpatterns = patterns(
    'api.views',
    url(r'^$', views.Root.as_view(), name='root'),

    url(r'^update/$', views.DeviceUpdateStatus.as_view(), name='device-update'),

    url(r'^user/profile/$', views.UserProfile.as_view(), name='user-profile'),

    url(r'^devices/$', views.DeviceList.as_view(), name='device-list'),
    url(r'^devices/(?P<pk>[0-9]+)/$', views.DeviceDetail.as_view(), name='device-detail'),

    url(r'^auth/$', views.AuthView.as_view(), name='authenticate')
)
