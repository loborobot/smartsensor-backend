from rest_framework import permissions
from api.models import Device, Sample


class IsOwner(permissions.BasePermission):
    """
    Custom permission to only allow owners of an device to edit it.
    """

    def has_object_permission(self, request, view, obj):
        if type(obj) == Device:
            return obj.owner == request.user
        if type(obj) == Sample:
            return obj.device.owner == request.user

        return False


class IsCurrentUser(permissions.BasePermission):
    """
    Custom permission to check if this user is the current authenticated user
    """

    def has_object_permission(self, request, view, obj):
        return obj.username == request.user.username