from django.contrib import admin
from api.models import Device, Sample


class DeviceAdmin(admin.ModelAdmin):
    list_display = ['id', '__unicode__']


class SampleAdmin(admin.ModelAdmin):
    pass

admin.site.register(Device, DeviceAdmin)
admin.site.register(Sample, SampleAdmin)
