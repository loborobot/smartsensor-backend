from django.contrib.auth.models import User
from rest_framework import serializers
from api.models import Device, Sample


class UserSerializer(serializers.ModelSerializer):
    date_joined = serializers.DateTimeField(required=False, read_only=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email',
                  'is_staff', 'is_active', 'date_joined')


class SampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = ('id','pub_date',
                  'temperature', 'humidity',
                  'light', 'ultra_violet',
                  'sound',
                  'flowmeter', 'volume',
                  'nitrogen_dioxide', 'carbon_monoxide',
                  'latitude', 'longitude')


class DeviceSerializer(serializers.ModelSerializer):
    samples = SampleSerializer(many=True, required=False)

    class Meta:
        model = Device
        fields = ('id','name', 'token', 'samples')


class UserProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True, required=False)
    
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class DeviceStatusSerializer(serializers.HyperlinkedModelSerializer):
    device = serializers.SlugRelatedField(slug_field='token')

    class Meta:
        model = Sample
        fields = ('id','device', 'pub_date', 'temperature', 'humidity',
                  'light', 'ultra_violet', 'sound', 'flowmeter',
                  'volume', 'nitrogen_dioxide', 'carbon_monoxide',
                  'latitude', 'longitude')
