from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Device(models.Model):
    owner = models.ForeignKey(User, related_name='devices')
    name = models.CharField(max_length=100)
    gsm = models.IntegerField()
    # MD5 sum of device MAC address
    token = models.CharField(max_length=32)
    ip = models.CharField(max_length=17)

    def __unicode__(self):
        return '%s (%s)' % (self.gsm, self.owner.username)


class Sample(models.Model):
    device = models.ForeignKey(Device, related_name='samples')
    pub_date = models.DateTimeField(default=datetime.now())
    temperature = models.DecimalField(max_digits=7, decimal_places=3)
    humidity = models.DecimalField(max_digits=4, decimal_places=2)
    light = models.DecimalField(max_digits=6, decimal_places=2)
    ultra_violet = models.DecimalField(max_digits=5, decimal_places=2)
    sound = models.DecimalField(max_digits=5, decimal_places=2)
    flowmeter = models.DecimalField(max_digits=5, decimal_places=2)
    volume = models.DecimalField(max_digits=5, decimal_places=2)
    nitrogen_dioxide = models.DecimalField(max_digits=10, decimal_places=2)     # TODO: pendiente
    carbon_monoxide = models.DecimalField(max_digits=10, decimal_places=2)      # TODO: pendiente
    latitude = models.DecimalField(max_digits=10, decimal_places=7)             # TODO: pendiente
    longitude = models.DecimalField(max_digits=10, decimal_places=7)            # TODO: pendiente

    def __unicode__(self):
        return '%s (%s)' % (self.device.gsm, self.pub_date)