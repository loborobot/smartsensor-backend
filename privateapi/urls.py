from django.conf.urls import patterns, url
from django.conf.urls import url
from rest_framework import routers
from django.conf.urls import patterns, include, url
from rest_framework import authentication, permissions
from rest_framework.response import Response
from django.core import serializers

from privateapi import views

from rest_framework import generics

from rest_framework.authtoken.views import obtain_auth_token



router = routers.SimpleRouter()
router.register(r'devices', views.DeviceViewSet)
router.register(r'^device', views.DeviceOneViewSet)
router.register(r'sensors', views.SensorViewSet)
router.register(r'histories', views.HistoryViewSet)
router.register(r'accesses', views.AccessViewSet)
router.register(r'schedulers', views.SchedulerViewSet)

#urlpatterns = router.urls
urlpatterns = patterns(
    'privateapi.views',
    url(r'^$', views.APIRoot.as_view()),
    url(r'', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',namespace='rest_framework')),
    url(r'^token/', obtain_auth_token, name='api-token'),
)

