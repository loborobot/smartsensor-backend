from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from rest_framework import generics, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
# Create your views here.
from django.conf.urls import url, include
from django.contrib.auth.models import User

from model.models import *

from rest_framework import routers, serializers, viewsets

import django_filters

class APIRoot(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    @staticmethod
    def get(request):
        return Response(

            {
                'devices':      "http://"+request.META['HTTP_HOST']+'/privateapi/devices/',
                'device':       "http://"+request.META['HTTP_HOST']+'/privateapi/device/',
                'sensors':      "http://"+request.META['HTTP_HOST']+'/privateapi/sensors/',
                'histories':    "http://"+request.META['HTTP_HOST']+'/privateapi/histories/',
                'accesses':     "http://"+request.META['HTTP_HOST']+'/privateapi/accesses/',
                'schedulers':   "http://"+request.META['HTTP_HOST']+'/privateapi/schedulers/',
            }  
            
        )

class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ( 'id','token', 'dnsname','port', 'state')
        
class DeviceViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer  
    
    
class DeviceOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ('id', 'token', 'dnsname','port', 'state')
        
class DeviceOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Device.objects.all()    
    serializer_class = DeviceSerializer    
    filter_fields = ('token',)    
            
            
class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = ('id', 'name', 'unity', 'minimum_range','maximum_range')
        
class SensorViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer


class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','value')
        
class HistoryViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = History.objects.all()
    serializer_class = HistorySerializer

class HistoryOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','value')
        
class HistoryOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = HistorySerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return History.objects.filter(device=deviceOne) 
    
class HistorySensorOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','value')
        
class HistorySensorOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = HistorySerializer
    def get_queryset(self,token_query,sensor_id): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        sensorOne=Sensor.objects.get(pk=sensor_id)
        return History.objects.filter(device=deviceOne,sensor=sensorOne) 
    
class AccessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Access.objects.all()
    serializer_class = AccessSerializer

class AccessOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = AccessOneSerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return Access.objects.filter(device=deviceOne) 
    
class AccessSensorOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Access
        fields = ('id', 'device', 'sensor', 'public')
        
class AccessSensorOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = AccessSerializer
    def get_queryset(self,token_query,sensor_id): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        sensorOne=Sensor.objects.get(pk=sensor_id)
        return Access.objects.filter(device=deviceOne,sensor=sensorOne) 
    
class SchedulerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scheduler
        fields = ('id', 'device','name','time_begin', 'time_end', 'date_begin','date_end','frecuency','rule','enabled','valve')
        
class SchedulerViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Scheduler.objects.all()
    serializer_class = SchedulerSerializer

class SchedulerOneSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scheduler
        fields = ('id', 'device','name','time_begin', 'time_end', 'date_begin','date_end','frecuency','rule','enabled','valve')
        
class SchedulerOneViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = SchedulerOneSerializer
    def get_queryset(self,token_query): ### this fails for token_query
        ## parameter in url .. review it
        deviceOne=Device.objects.filter(token=token_query)
        return Scheduler.objects.filter(device=deviceOne)
 

    