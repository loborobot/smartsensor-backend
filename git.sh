cd libs && rm -rf *.pyc && rm -rf *.py~
cd ..
cd model && rm -rf *.pyc && rm -rf *.py~
cd ..
cd privateapi && rm -rf *.pyc && rm -rf *.py~
cd ..
cd webapi && rm -rf *.pyc && rm -rf *.py~
cd ..
cd tokenapi && rm -rf *.pyc && rm -rf *.py~
cd ..
cd smartsensor && rm -rf *.pyc && rm -rf *.py~
cd ..
cd web && rm -rf *.pyc && rm -rf *.py~
cd ..
rm -rf *.pyc
cd web/static && rm -rf bower_components/
cd ../..
rm -rf node_modules/
cd web
rm -rf node_modules/
cd ..
git reset HEAD *
git add . -f
git commit -am "update con fields minimos"
git push origin deploymentv3