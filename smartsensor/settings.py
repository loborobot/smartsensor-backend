import os
import dj_database_url

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

SECRET_KEY = 'p5p*@!3!%iq=u@@+3s#q8@!mv223%-551f8p*3!7z+-h%_mhnu'

# Remote key to production server to smartsensor.herokuapp.com
APIKEY_GOOGLE_MAPS = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyBllMk4ZnyayVGI0RQlw4udS-Vqw58bcEQ&sensor=TRUE'

# Remote key to testing smartsensor-test.herokuapp.com it must be uncomment if you are testing a production server
# APIKEY_GOOGLE_MAPS = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyB-K7T6RxceKC31qAy4C5E8a2dxSiEFvik&sensor=TRUE'

DEBUG = os.getenv('DJANGO_ENV', 'dev') != 'prod'

TEMPLATE_DEBUG = True

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

ALLOWED_HOSTS = []

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    ###'api',
    'web',
    'model',
    'webapi',
    'privateapi'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    # 'DEFAULT_MODEL_SERIALIZER_CLASS': 'rest_framework.serializers.HyperlinkedModelSerializer',
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),
    #'PAGINATE_BY': 10,
    #'PAGINATE_BY_PARAM': 'page_size',
    'DEFAULT_PERMISSION_CLASSES': (
        #'rest_framework.permissions.IsAuthenticated',
        'rest_framework.permissions.AllowAny',
    ),

}

TEMPLATE_CONTEXT_PROCESSORS = (
  'django.contrib.auth.context_processors.auth',
  'webapi.context_processors.vars_settings',
)

ROOT_URLCONF = 'smartsensor.urls'

WSGI_APPLICATION = 'smartsensor.wsgi.application'

DATABASES = {'default': dj_database_url.config()}      # heroku database

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = 'staticfiles'
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PHOTO', 'https')

ALLOWED_HOSTS = ['*']

FIXTURE_DIRS = (
   os.path.join(BASE_DIR, '../fixtures/'),
)

try:
  from local_settings import *
except ImportError:
  print """
    -------------------------------------------------------------------------
    If you see this message you need to create a local_settings.py file 
    which needs to contain at least database connection information.
               
    Copy local_settings_example.py to local_settings.py and edit it.
    -------------------------------------------------------------------------
    """

