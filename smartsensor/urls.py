from django.conf.urls import patterns, include, url
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),

    #url(r'^api/', include('api.urls')),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^', include('web.urls')),
    
    url(r'^webapi/', include('webapi.urls')),
    #url(r'^webapi/update/^$', views.update),
    url(r'^privateapi/', include('privateapi.urls')),
    url(r'^tokenapi/', include('tokenapi.urls')),

)