import os
from settings import SITE_ROOT

DEBUG = True
TEMPLATE_DEBUG = DEBUG

# api para google maps para desarrollo
APIKEY_GOOGLE_MAPS = 'http://maps.googleapis.com/maps/api/js?key=AIzaSyCfy76oSiLGD9ffbD-nbg8EFVEVHt5LKpE&sensor=TRUE'

# Por favor poner aqui sus configuraciones locales
DATABASES = {
     'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'smartcity',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

