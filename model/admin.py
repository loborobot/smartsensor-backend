from django.contrib import admin

# Register your models here.


from model.models import *


class LoginAdmin(admin.ModelAdmin):
    pass



class DeviceAdmin(admin.ModelAdmin):
    pass

class OwnerDeviceAdmin(admin.ModelAdmin):
    pass

class SensorAdmin(admin.ModelAdmin):
    pass

class HistoryAdmin(admin.ModelAdmin):
    pass

class AccessAdmin(admin.ModelAdmin):
    pass

class SchedulerAdmin(admin.ModelAdmin):
    pass


admin.site.register(Login, LoginAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(OwnerDevice, OwnerDeviceAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(History, HistoryAdmin)
admin.site.register(Access, AccessAdmin)
admin.site.register(Scheduler, SchedulerAdmin)

    

    
