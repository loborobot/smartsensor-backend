from django.db import models
import datetime

from django.contrib.auth.models import User
from datetime import datetime

from django.test.client import encode_multipart, RequestFactory

import json

import httplib
import urllib
import urllib2

from libs.http import Request

from json import dumps, loads, JSONEncoder


# Create your models here.

class Login(models.Model): ## User ya esta reservado en django
    name = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    nickname = models.CharField(max_length=200, unique=True)
    password = models.CharField(max_length=100)
    email = models.CharField(max_length=300)
    token = models.CharField(max_length=200)
    
    def save(self, *args, **kwargs):        
        super(Login, self).save(*args, **kwargs)
        try:        
            user=User.objects.get(username=self.nickname) 
        except User.DoesNotExist:
            user =  User(
                        username=self.nickname,
                        first_name=self.name,
                        last_name=self.lastname,
                        email=self.email,
                        password=self.password,
                        is_staff=False,
                        is_active=True,
                        is_superuser=False,
                     )  
            user.save()
        
        
    def __unicode__(self):
        return '%s , %s' % (self.name, self.lastname)




class Device(models.Model):
    token = models.CharField(max_length=32)
    name = models.CharField(max_length=32)
    dnsname = models.CharField(max_length=200)
    port = models.IntegerField(max_length=5,default=8080)
    date_register = models.DateTimeField(default=datetime.now())
    city = models.CharField(max_length=200)
    country = models.CharField(max_length=200)
    latitude= models.DecimalField(max_digits=23,decimal_places=20,default=0.0)
    longitude= models.DecimalField(max_digits=23,decimal_places=20,default=0.0)
    state = models.CharField(max_length=8)
    type_network = models.CharField(max_length=20, default='wireless')
    sim_number = models.CharField(max_length=20, default='000000000000')

    def save(self, *args, **kwargs):
        super(Device, self).save(*args, **kwargs) 
        r=Request()
        try:
            data=r.put(self.dnsname,{'state':self.state})
        except:
            print(" -- No se pudo acceder a metodo put --")
            pass
        
    def senddata(self):
        r=Request()
        objects=Scheduler.objects.get(device=self)
        structure= {'CMD':'UPDATE_SCHEDULERS','schedulers':objects}
        data = json.loads(structure)
        try:
            data=r.put(self.dnsname,data)
        except:
            print(" -- No se pudo acceder a metodo put --")
            pass
        
    # sin longitud el estado del dispositivo puede cambiar de longitud     
    def __unicode__(self):
        return '%s (%s)' % (self.token,self.dnsname)
    
    

class OwnerDevice(models.Model):
    owner = models.ForeignKey(Login, related_name='owner')
    device = models.ForeignKey(Device, related_name='devices')
    deleted = models.IntegerField(max_length=1,default=0)
    def __unicode__(self):
        return '%s (%s)' % (self.owner.nickname,self.device.token)
    
class Sensor(models.Model):
    name=models.CharField(max_length=200)
    unity=models.CharField(max_length=200)
    minimum_range=models.DecimalField(max_digits=8,decimal_places=2)
    maximum_range=models.DecimalField(max_digits=8,decimal_places=2)
    def __unicode__(self):
        return '( %s )' % (self.name)

class History(models.Model):
    device = models.ForeignKey(Device, related_name='histories')
    sensor = models.ForeignKey(Sensor, related_name='sensors')
    date=models.DateTimeField(default=datetime.now())
    value=models.DecimalField(max_digits=8,decimal_places=2)
    
    def __unicode__(self):
        return '( %s ) -- %s ' % (self.device.token,self.sensor.name)
    
class Access(models.Model):
    device = models.ForeignKey(Device, related_name='accesses')
    sensor = models.ForeignKey(Sensor, related_name='public_accesses')
    public = models.CharField(max_length=5)
    
    def __unicode__(self):
        return '( %s ) -- %s ' % (self.device.token,self.sensor.name)
    
    
class Scheduler(models.Model):
    device = models.ForeignKey(Device, related_name='schedulers')
    name= models.CharField(max_length=300)
    time_begin= models.TimeField()
    time_end=   models.TimeField()
    date_begin= models.DateTimeField()
    date_end= models.DateTimeField()
    frecuency = models.CharField(max_length=5)
    rule=models.CharField(max_length=200)
    enabled=models.BooleanField(default=True)
    remote_port=models.IntegerField(max_length=3,default=1)

    def __unicode__(self):
        return '( %s ) ' % (self.device.token)
    
    def save(self, *args, **kwargs):    
        super(Scheduler, self).save(*args, **kwargs) 
        try:
            self.device.senddata()
        except:
            pass