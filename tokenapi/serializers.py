from django.contrib.auth.models import User
from rest_framework import serializers
from model.models import Login,Device,OwnerDevice,Sensor,History,Access,Scheduler





class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = (
                        'token',
                        'name',
                        'dnsname',
                        'date_register',
                        'city',
                        'country',
                        'latitude',
                        'longitude',
                        'state'
                )

class SensorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sensor
        fields = (  
                        'id', 
                        'name', 
                        'unity', 
                        'minimum_range',
                        'maximum_range'
                )

class HistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = History
        fields = ('id', 'device', 'sensor', 'date','value')


class SchedulerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scheduler
        fields = ('id', 'device','name','time_begin', 'time_end', 'date_begin','date_end','frecuency','rule','enabled','valve')