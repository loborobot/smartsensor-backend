from rest_framework import permissions

from model.models import Login,Device,OwnerDevice,Sensor,History,Access,Scheduler

from django.db import models


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        token=request.token
        user=Login.objects.get(token=token)        
        if type(obj) == Device:
            ownerdevice=OwnerDevice.objects.get(device=obj)
            return ownerdevice.owner == user
        if type(obj) == History:
            ownerdevice=OwnerDevice.objects.get(device=obj.device)
            return ownerdevice.owner == user
        if type(obj) == Scheduler:
            ownerdevice=OwnerDevice.objects.get(device=obj.device)
            return ownerdevice.owner == user
        return False


class IsCurrentUser(permissions.BasePermission):
    """
    Custom permission to check if this user is the current authenticated user
    """

    def has_object_permission(self, request, view, obj):
        return obj.username == request.user.username