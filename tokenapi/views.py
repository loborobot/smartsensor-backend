from datetime import datetime

from django.utils.encoding import force_bytes
from django.core import serializers

try:
    import json
except ImportError:
    from django.utils import simplejson as json

from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import login, logout, authenticate
from django.core import serializers
from django.conf.urls import url, include
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render

from rest_framework import generics, permissions
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import authentication, permissions
from rest_framework import mixins

from rest_framework import routers, serializers, viewsets

import django_filters

from libs.http import Request

import json
import requests

from django.core.paginator import Paginator
from rest_framework import filters
from rest_framework import generics

from django.views.decorators.csrf import csrf_exempt

#from django.core.paginator import Paginator , EmptyPage, PageNotAnInteger
#from api.serializers import PaginatedUserSerializer, UserKarmaSerializer
#from rest_framework import pagination
#from rest_framework_extensions.mixins import PaginateByMaxMixin

#import logging
#logging.basicConfig(level=logging.DEBUG)
#logger = logging.getLogger(__name__)

from model import models
from serializers import *
from . import permissions

from rest_framework import generics, permissions
from model.models import *



from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status





class APIRoot(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    @staticmethod
    def get(request):
        return Response(

            {
                'devicetoken':       "http://"+request.META['HTTP_HOST']+'/tokenapi/devicetoken/',
            }  
            
        )



@csrf_exempt    
def login(request):    
    user = request.POST['user']
    password= request.POST['password']    
    data=Login.objects.get(nickname=user,password=password)     
    nosuccess={'success':'false','token':''}
    response=HttpResponse(json.dumps(nosuccess))
    token=None
    if data is not None:   
        token=data.token

    if token is not None: 
        success={'success':'true','token':token}
        response= HttpResponse(json.dumps(success), content_type='application/json')
    return response


@csrf_exempt    
def devicetoken(request):    
    token = request.GET['token']
    #print token
    user=None
    if token is not None:
        user=Login.objects.filter(token=token)
    #print user
    owner=[]
    if user is not None:        
        owners = OwnerDevice.objects.filter(owner=user,deleted=0)
    #print json.dumps(owner)
    devices = []
    counter=0
    #print owners
    if owners is not None:
        for item in owners:
            devices.append({
                            'id':force_bytes('%s'%item.device.id),
                            'token':item.device.token,
                            'name':item.device.name,
                            'dnsname':item.device.dnsname,
                            'port':force_bytes('%s'%item.device.port),
                            'date_register':force_bytes('%s'%item.device.date_register),
                            'city':item.device.city,
                            'country':item.device.country,
                            'latitude':force_bytes('%s'%item.device.latitude),
                            'longitude':force_bytes('%s'%item.device.longitude)
                            }
                           )                
    nosuccess={'success':'false'}
    response=HttpResponse(json.dumps(nosuccess))
    if devices is not None: 
        response= HttpResponse(json.dumps(devices), content_type='application/json')
    return response



    #scheduler
    #device,name,time_begin,time_end,date_begin,date_end,frecuency,rule,enabled,valve

@csrf_exempt    
def schedulertoken(request):    
    token = request.GET['token']
    #print token
    user=None
    if token is not None:
        user=Login.objects.filter(token=token)
    #print user
    owner=[]
    if user is not None:        
        owners = OwnerDevice.objects.filter(owner=user,deleted=0)
    #print json.dumps(owner)
    schedulers = []
    list=[]
    counter=0
    #print owners
    if owners is not None:
        for item in owners:
            list=Scheduler.objects.filter(device=item.device)
            if list is not None:
                for it in list:
                    schedulers.append(
                                        {
                                            'id':force_bytes('%s'%it.id),
                                            'device':force_bytes('%s'%it.device.token),
                                            'name':it.name,
                                            'time_begin':force_bytes('%s'%it.time_begin),
                                            'time_end':force_bytes('%s'%it.time_end),
                                            'date_begin':force_bytes('%s'%it.date_begin),
                                            'date_end':force_bytes('%s'%it.date_end),
                                            'frecuency':force_bytes('%s'%it.frecuency),
                                            'rule':force_bytes('%s'%it.rule),
                                            'enabled':force_bytes('%s'%it.enabled),
                                            'valve':force_bytes('%s'%it.valve)
                                         }
                                      )
    nosuccess={'success':'false'}
    response=HttpResponse(json.dumps(nosuccess))
    if schedulers is not None: 
        response= HttpResponse(json.dumps(schedulers), content_type='application/json')
    return response

@csrf_exempt    
def sensorstoken(request):    
    id = request.GET['id']
    #print token
    device=None
    if id is not None:
        device=Device.objects.filter(id=id)
    histories=[]
    #print device
    data = []
    all = []
    if device is not None:        
        histories = History.objects.filter(device=device)
    
    if device is not None:
        #print histories
        for item in histories:
            try:                
                if all[item.sensor.id] is not None:
                    all.remove(all[item.sensor.id])
            except:
                print '--'
            all.insert(item.sensor.id,item)
            #print '\n %s \n'%item.sensor.id                
            
    if device is not None:
        for item in all:
            data.append(
                         {
                            'id':force_bytes('%s'%item.device.id),
                            'device':force_bytes('%s'%item.device.token),
                            'sensor':force_bytes('%s'%item.sensor.id),
                            'date':force_bytes('%s'%item.date),
                            'value':force_bytes('%s'%item.value)
                         }
                       )                
    nosuccess={'success':'false'}
    response=HttpResponse(json.dumps(nosuccess))
    if data is not None: 
        response= HttpResponse(json.dumps(data), content_type='application/json')
    return response

def reverse_string(a_string):
    return a_string[::-1]

@csrf_exempt    
def schedulerupdate(request):    
    token = request.GET['token']
    id = request.GET['id']
    enable=True
    try:
        enabled = request.GET['enabled']
    except:
        enabled = 'true'
    if(enabled=='true'):
        enable=True
    else:
        enable=False

    user=None
    if token is not None:
        user=Login.objects.filter(token=token)
    owner=[]
    if user is not None:        
        owners = OwnerDevice.objects.filter(owner=user,deleted=0)
    schedulers = []
    list=[]
    counter=0
    #print owners

    response = {'success':'false'}
    success = {}
    #response=HttpResponse(json.dumps(nosuccess))

    if owners is not None:
        for item in owners:
            # list of schedulers
            scheduler = Scheduler.objects.filter(device=item.device,id=id)
            schedulers = Scheduler.objects.all()

            binary_number = ''

            if scheduler is None:
                response = {'msg': 'Does not exits!', 'success': 'false'}
                HttpResponse(json.dumps(response), content_type='application/json')
            else:

                for elem in scheduler:
                    elem.enabled = enable
                    elem.save()

                for s in schedulers:
                    # enviamos un codigo decimal al serializador que convierte a binario del estado de todas las
                    # valvulas
                    # 0000 0000 trabajamos con el bit mas a la derecha y con solo 4 bits
                    # b7 b6 b5 b4 b3 b2 b1 b0


                    if s.remote_port == 1:  # tener en cuenta que no se puede tener mas de 3 valvulas y se usaran 3 bits
                        if s.enabled:
                            binary_number += '1'
                        else:
                            binary_number += '0'
                    elif s.remote_port == 2:
                        if s.enabled:
                            binary_number += '1'
                        else:
                            binary_number += '0'
                    elif s.remote_port == 3:
                        if s.enabled:
                            binary_number += '1'
                        else:
                            binary_number += '0'
                    elif s.remote_port == 4:
                        if s.enabled:
                            binary_number += '1'
                        else:
                            binary_number += '0'

                #tmp = reverse_string(binary_number)
                status_decimal = int(binary_number, 2)

                data = {
                        'cmd': 1,# codigo de comunicacion
                        'po':elem.remote_port,
                        'st': status_decimal, # decimal del estado del array de valvulas
                        'ts':str(elem.time_begin),
                        'te':str(elem.time_end),
                        'fq':elem.rule
                        }

                if elem.device.type_network == 'wireless':
                    url = ''.join(['http://', elem.device.dnsname, ':', str(elem.device.port)])
                    headers = {'Content-Type': 'application/json', 'User-Agent': 'python'}

                    response = requests.post(url, data=json.dumps(data), headers=headers)

                    #logger.debug("posting remote " + url)
                    #logger.debug(response)

                    if response.status_code == 200:
                        success={'success': 'true', 'msg': 'Updated port'}
                    else:
                        success={'success':'false', 'msg': 'Cant updated port'} #the remote device send 404 error code that means it couldn't receive data

    return HttpResponse(json.dumps(success), content_type='application/json')
