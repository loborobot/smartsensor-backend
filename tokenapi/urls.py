from django.conf.urls import patterns, url
from django.conf.urls import url
from rest_framework import routers
from django.conf.urls import patterns, include, url
from rest_framework import authentication, permissions
from rest_framework.response import Response
from django.core import serializers

from tokenapi import views

from rest_framework import generics

from django.conf.urls import patterns, url



urlpatterns = patterns(
    'tokenapi.views',
    url(r'^$', views.APIRoot.as_view()),
    #url(r'^devicetoken/(?P<token>[a-zA-Z0-9]+)/$', views.DeviceToken.as_view()),
    url(r'^login', 'login'),
    url(r'^devicetoken', 'devicetoken'),
    url(r'^schedulertoken', 'schedulertoken'),
    url(r'^sensorstoken', 'sensorstoken'),
    url(r'^schedulerupdate', 'schedulerupdate'),

)