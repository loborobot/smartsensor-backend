'use strict';


function obtenerData(id) {
	var data = [];
	data['fecha'] = "Hoy";
	data['temperatura'] = '33';
	data['humedad'] = '23';
	data['luz'] = '5000';
	data['dioxido'] = '5000';
	data['monoxido'] = '4500';
	return data;
}

function message(id, info) {
	//console.log(info);
	try {
		infowindow.close();
	} catch(ex) {
	}
	var point;
	for (var i in markers) {
		if (markers[i].getZIndex() == id) {
			point = markers[i];
		}
	}
	//console.log(map);
	var fields = ['date', 'temperature', 'humidity', 'ultra_violet', 'sound', 'flowmeter', 'light', 'nitrogen_dioxide', 'carbon_monoxide', 'latitude', 'longitude'];
	var d = '';
	var message = '';
	for (var f in fields) {
		d = '';
		try {
			d = info[fields[f]];
		} catch(ex) {
			d = '';
		}
		//console.log(d);
		try {
			$('#' + fields[f]).html(d);
		} catch(ex) {
		}
	}

	message = '';
	try {
		message = $('#info').html();
	} catch(ex) {
	}
	try {
		infowindow = new google.maps.InfoWindow({
			content : message
		});
	} catch(ex) {
	}
	infowindow.setContent(message);
	try {
		infowindow.open(map, point);
	} catch(ex) {
	}

    try{
        $("#flowmeterdiv").click();
    }catch(ex){}
}

function createMarker(map, myLatLng, id) {
	var marker = new google.maps.Marker({
		position : {
			lat : myLatLng.lat(),
			lng : myLatLng.lng()
		},
		title : "Marker",
		draggable : false,
		animation : google.maps.Animation.DROP

	});
	marker.setMap(map);
	marker.setZIndex(id);

	try {
		google.maps.event.addListener(marker, 'click', toggleBounce);
	} catch(ex) {
	}
	return marker;
}

function toggleBounce(marker) {
	if (marker.getAnimation() != null) {
		try {
			marker.setAnimation(null);
		} catch(ex) {
		}
	} else {
		try {
			marker.setAnimation(google.maps.Animation.BOUNCE);
		} catch(ex) {
		}
	}
	try {
		data = obtenerData(marker.getZIndex());
	} catch(ex) {
	}
}

function createMap(myLatLng) {
	mapOptions = {
		center : {
			lat : myLatLng.lat(),
			lng : myLatLng.lng()
		},
		zoom : 11
	};

	divMap = document.getElementById('map');
	map = new google.maps.Map(divMap, mapOptions);

	return map;
}

function initialize() {

	try {
		myLatLng = new google.maps.LatLng(-16.41262, -71.5375276);
	} catch(ex) {
	}

	map = createMap(myLatLng);

}

function loadDataMarkers(dataMap)
{	
	if(dataMap){
		dataMap.shift();
		for (var i in dataMap) {
			var index = parseInt(i);
			if (index == i) {
				try{
					myLatLng = new google.maps.LatLng(
									dataMap[index]['latitude'], 
									dataMap[index]['longitude']
								);	
				}catch(ex){}
				//console.log(dataMap[index]['latitude']);
				//console.log(dataMap[index]['longitude']);
				try{
					marker = new google.maps.Marker({
						position : {
							lat : myLatLng.lat(),
							lng : myLatLng.lng()
						},
						title : "Token: "+dataMap[index]['token'],
						draggable : false,
						animation : google.maps.Animation.DROP
		
					});
					//console.log(marker);
				}catch(ex){}
				try{
					marker.setMap(map);
				}catch(ex){}
				try{
					marker.setZIndex(dataMap[index]['id']);
				}catch(ex){}
				try{
					markers[dataMap[index]['id']] = marker;
				}catch(ex){}
				
				google.maps.event.addListener(marker, 'click', function() {
					
					var id = this.getZIndex();				
					$("#infochart").data("deviceid",id);
					$("#infochart").data("markerid",id);
					$("#infochart").data("sensorid",0);	
					
					var info = new Array();
					//console.log("datMap");
					//console.log(dataMap);
					for (var i in dataMap) {
						if (dataMap[i]['id'] == id) {
							info = dataMap[i];
						}	
					}
					this.setAnimation(google.maps.Animation.BOUNCE);
//                    console.log(info);
					message(id, info);
					var params = new Array();
					var fields = ['date', 'temperature', 'humidity',  'sound', 'flowmeter', 'light', 'nitrogen_dioxide', 'carbon_monoxide', 'latitude', 'longitude'];
					for (var f in fields) {
						params[fields[f]] = new Array();
						for (var j in info['samples'] ) {
							var sample="";
							try{
								sample = info['samples'][j];
							}catch(ex){}
							if(sample){
								params[fields[f]].push(sample);	
							}							
						}
						//console.log(JSON.stringify(info));
					}
				});
				data.push(dataMap[i]);
				//console.log(data);
			}
		}	
	}
	
	
}

