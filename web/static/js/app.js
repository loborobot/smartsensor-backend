'use strict';


/** Angular Bootstrap **/
angular.module('app', [
    'ngRoute',
    'ngResource',
    'ngCookies',
    'app.filters',
    'app.services',
    'app.models',
    'app.directives',
    'app.controllers'
])

/** Routing **/
    .config(['$routeProvider', function ($routeProvider) {
        var viewBase = '/static/views/';

        $routeProvider
            .when('/login', {
                controller: 'AuthController',
                templateUrl: viewBase + 'login.html',
                isFree: true
            })

            .when('/', {
                controller: 'MainController',
                templateUrl: viewBase + 'main.html'
            })

                .when('/profile', {
                controller: 'ProfileController',
                templateUrl: viewBase + 'profile.html'
            })

            .otherwise({
                templateUrl: viewBase + '404.html'
            });
    }])

/** Auth Config **/
    /*.config(['$httpProvider', function ($httpProvider) {
        // security headers
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

        // intercept forbidden requests
        $httpProvider.interceptors.push(function($q, $location) {
            return {
                'responseError': function (response) {
                    if (response.status === 401 || response.status === 403) {
                        $location.path('/login');
                    }
                    return $q.reject(response);
                }
            };
        });
    }]);*/

/** Django Integration **/
    .config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
    }])

/** Set-Up Security **/
    .run(['$rootScope', '$location', 'auth', function ($rootScope, $location, auth) {
        $rootScope.$on('$routeChangeStart', function (event, next, current) {
            // skip in login page or free pages
            if (next.isFree || next.originalPath == '/login') return;
            else if (!auth.isAuthenticated()) {
                event.preventDefault();
                $location.path('/login');
            }
        });
    }])
;
