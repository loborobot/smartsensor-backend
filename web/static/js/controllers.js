'use strict';

/* Controllers */
//angularCharts
angular.module('app.controllers', [])

/** Control the main navBar behavior **/.controller('MenuController', ['$scope', '$location', 'auth',
function($scope, $location, auth) {
	$scope.user = auth.user;

	$scope.isAuthenticated = function() {
		return auth.isAuthenticated();
	};

	$scope.isActive = function(route) {
		return route == $location.path();
	};

	$scope.logout = function() {
		auth.logout().then(function() {
			$location.path('/login');
		});
	};
}])

/** Controller for authentication services **/.controller('AuthController', ['$scope', '$location', 'auth',
function($scope, $location, auth) {

	if (auth.isAuthenticated())
		$location.path('/');

	$scope.login = function() {
		auth.login($scope.user).then(function() {
			$location.path('/');
		});
	};
}])

/**********************
 *  Routing Controllers
 **********************/

/**
 * MainController
 *
 * show welcome message and register/login forms
 *
 * @route:      #/
 * @view:       views/main.html
 **/.controller('MainController', ['$scope', 'Device',
function($scope, Device) {
	Device.query().$promise.then(function(devices) {
		$scope.devices = devices;
		//console.log(devices);
	});
	/*
	 $scope.data = {
	 labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
	 datasets: [
	 {
	 //label: '',
	 fillColor: 'rgba(220,220,220,0.2)',
	 strokeColor: 'rgba(220,220,220,1)',
	 pointColor: 'rgba(220,220,220,1)',
	 pointStrokeColor: '#fff',
	 pointHighlightFill: '#fff',
	 pointHighlightStroke: 'rgba(220,220,220,1)',
	 data: [65, 59, 80, 81, 56, 55, 40]
	 },
	 {
	 //label: '',
	 fillColor: 'rgba(151,187,205,0.2)',
	 strokeColor: 'rgba(151,187,205,1)',
	 pointColor: 'rgba(151,187,205,1)',
	 pointStrokeColor: '#fff',
	 pointHighlightFill: '#fff',
	 pointHighlightStroke: 'rgba(151,187,205,1)',
	 data: [28, 48, 40, 19, 86, 27, 90]
	 }
	 ]
	 };*/

	/*

	 $scope.options =  {

	 // Sets the chart to be responsive
	 responsive: true,

	 ///Boolean - Whether grid lines are shown across the chart
	 scaleShowGridLines : true,

	 //String - Colour of the grid lines
	 scaleGridLineColor : "rgba(0,0,0,.05)",

	 //Number - Width of the grid lines
	 scaleGridLineWidth : 1,

	 //Boolean - Whether the line is curved between points
	 bezierCurve : true,

	 //Number - Tension of the bezier curve between points
	 bezierCurveTension : 0.4,

	 //Boolean - Whether to show a dot for each point
	 pointDot : true,

	 //Number - Radius of each point dot in pixels
	 pointDotRadius : 4,

	 //Number - Pixel width of point dot stroke
	 pointDotStrokeWidth : 1,

	 //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	 pointHitDetectionRadius : 20,

	 //Boolean - Whether to show a stroke for datasets
	 datasetStroke : true,

	 //Number - Pixel width of dataset stroke
	 datasetStrokeWidth : 2,

	 //Boolean - Whether to fill the dataset with a colour
	 datasetFill : true,

	 // Function - on animation progress
	 onAnimationProgress: function(){},

	 // Function - on animation complete
	 onAnimationComplete: function(){},

	 //String - A legend template
	 legendTemplate : '<ul class="tc-chart-js-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].strokeColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>'
	 };*/

}])

/**
 * ProfileController
 *
 * Show/Update User profile
 *
 * @route:      #/profile
 * @view:       profile.html
 */
.controller('ProfileController', ['$scope', 'User',
function($scope, User) {
	User.get().$promise.then(function(user) {
		$scope.user = user;
	});
}])
.controller('MarkersController', ['$scope', 'Device', 'Sensor', 'Access', 'History',
function($scope, Device, Sensor, Access, History) {

	initialize();

	try{
		Sensor.query().$promise.then(
			function(sensores) {
				sensors=sensores;				
			}
		);	
	}catch(ex){}
	
	try{devices = Device.query();}catch(ex){}	
	
	try{access = Access.query();}catch(ex){}	
	
  // esta funcion deberia retorna el ultimo dato de la base de datos 
  // y no recorrer todos los histories desde 0 hasta los n que haya, ya 
  // que si en una semana se genera 10mil recorrera los 10mil para entregar solo el ultimo. 
  // debe de hacer el query del ultimo o los 5 ultimos para recorrer solo 5
  // ahora funciona porque no esta cargada la base de datos pero en una semana este
  // marker se volvera pesado.
	History.query().$promise.then(
		function(histories) {
			var sensor = '';
			var n;
			var device;
			var latitude;
			var longitude;
			var token;
			var value;
			var date;
			
			for(var i=0; i< histories.length; ++i) {
				
				n=parseInt(i);
				if(i==n){
					//console.log( sensors[ histories[i]['sensor'] ]['name'] );
					//console.log(sensors);
					sensor='';
					try{
						if(histories[i]['sensor']){
				            for(var j=0; j<sensors.length; ++j)
				            {
				              if(sensors[j]['id']==histories[i]['sensor'])
				              {
								sensor=sensors[j]['name'];	
				                value = histories[i]['value'];
				      			date  = histories[i]['date'];
								break;
							  }
				            }							
              								
						}				
					}catch(ex){
						sensor='nan';
					}
					
          			date=date.toString();
					date= date.replace("T"," ");
					date= date.replace("Z"," ");
					 
					for(var k = 0; k < devices.length; k++) {
						if (histories[i]['device'] == devices[k]['id']) {
							device = devices[k]['id'];
							token = devices[k]['token'];
							latitude = devices[k]['latitude'];
							longitude = devices[k]['longitude'];
						}
					}	

					if(!dataMap[ device ]){
						dataMap[ device ] = new Array();
						dataMap[ device ]['device'] = device;
						dataMap[ device ]['id'] = device;
						dataMap[ device ]['token'] = token;
						dataMap[ device ]['latitude'] = latitude;
						dataMap[ device ]['longitude'] = longitude;
						//dataMap[ device ]['date'] = date;	
					}						
					if(sensor){
						dataMap[ device ][sensor] = value;
            			dataMap[ device ]['date'] = date;						
					}	
					//console.log("sensor");
					//console.log(sensor);			
				}		
			}
			console.log(dataMap);
			loadDataMarkers(dataMap);
		}
	);
	
	Sensor.query().$promise.then(
		function(sensores) {
			try{
				for(var i in sensores){
					sensors.push(sensores[i]);
				}
				//sensors=sensores;
			}catch(ex){}
								
			var datasensors = new Array();
			
			var arraysensors = new Array();
			
			var name = "";
			for (var t = 0; t < sensors.length; t++) {
				name = "";
				if(sensors[t]['id']){
					try{
						name=sensors[t]['name'];					
					}catch(ex){}
					arraysensors[sensors[t]['id']]=new Array();
					try{
						arraysensors[sensors[t]['id']]['name']=sensors[t]['name'];
					}catch(ex){}
					try{
						arraysensors[sensors[t]['id']]['id']=sensors[t]['id'];	
					}catch(ex){}
					
				}
			}
			arraysensors.shift();
			//console.log(sensors);
			for (var index in arraysensors) {
				name="";
				try{
					name=arraysensors[index]['name'];					
				}catch(ex){}
				try{
					id=arraysensors[index]['id'];					
				}catch(ex){}
				try{
					name = name.replace("_", "");
				}catch(ex){}
				try{
					datasensors[name] = arraysensors[index]['id'];	
				}catch(ex){}				
			}
			//console.log(arraysensors);
			//console.log(datasensors);
			try{
				loadEventsChart(datasensors);
			}catch(ex){}
				
		}
	);
}]);

