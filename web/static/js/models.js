'use strict';

/* Models */

angular.module('app.models', [])
    .factory('User', ['$resource', function ($resource) {
        return $resource('api/user/profile/ ', {}, {
            update: {method: 'put'}
        });
    }])
    .factory('Device', ['$resource', function ($resource) {
        return $resource('webapi/devices/?format=json& ');
    }])
    .factory('Sensor', ['$resource', function ($resource) {
        return $resource('webapi/sensors/?format=json& ');
    }])
    .factory('History', ['$resource', function ($resource) {
        return $resource('webapi/histories/?format=json& ');
    }])
    .factory('Access', ['$resource', function ($resource) {
        return $resource('webapi/accesses/?format=json& ');
    }])
	.factory('Scheduler', ['$resource', function ($resource) {
        return $resource('webapi/schedulers/?format=json& ');
    }])
;
