'use strict';

/* Services */

angular.module('app.services', [])
    .factory('auth', ['$http', '$rootScope', '$q', '$cookieStore', function ($http, $rootScope, $q, $cookieStore) {

        // TODO: Fix $cookieStore auto JSON parse
        var currentUser = angular.fromJson($cookieStore.get('user')) || { username: '' };

        $cookieStore.remove('user');

        var changeUser = function (user) {
            angular.extend(currentUser, user);
        };

        return {
            isAuthenticated: function () {
                return currentUser.username !== '';
            },

            login: function (user) {
                var deferred = $q.defer();

                $http.post('/api/auth/', {user: user}).then(
                    function (response) {
                        changeUser(response.data);
                        deferred.resolve(response.data);
                    },
                    function (response) {
                        deferred.reject(response.data.error);
                    }
                );

                return deferred.promise;
            },

            logout: function (success, error) {
                var deferred = $q.defer();

                $http.delete('/api/auth/').then(
                    function () {
                        changeUser({username: ''});
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject(error.data.error);
                    }
                );

                return deferred.promise;
            },
            user: currentUser
        };
    }])
;
