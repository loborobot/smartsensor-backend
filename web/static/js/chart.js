var chart;
var base_url='/';

var line;

var sensorvars= [
				'temperature',
				'humidity',
				'sound', 
				'flowmeter',
				'light',
				'nitrogendioxide',			
				'carbonmonoxide'				 
			];
			
function sensordevice(sensorid,deviceid){	
	var size=10;
	var dataurl=base_url+'webapi/pagedhistories/?device='+deviceid+'&sensor='+sensorid+'&page_size='+size+'&format=json';
	console.log(dataurl);
	$.ajax(
		{
			url: dataurl,
			beforeSend: function( xhr ) {
				xhr.overrideMimeType( "application/json; charset=UTF-8" );
			}
		}
	).done(
			function( data ) {
				var fechas=new Array();
				var values=new Array();
				for(var i=0;i<data.length;i++){
					fechas[i]=data[i]['date'];
					values[i]=data[i]['value'];
				}
				singlechart(fechas,values);
			}
	);
}

function singlechart(datax,datay){
	var data=new Array();
	for(var i =0;i<datax.length;i++){
		data.push({ y: datax[i], a: datay[i] });		
	}
	$("chart").html("");
	if(!line){
		line=Morris.Line(
			{
		  		element: 'chart',
		  		data: data,
		  		xkey: 'y',
		  		ykeys: ['a'],
		  		labels: ['Series A']
			}
		);
	}else{
		line.setData(data);
	}
}

function loadEventsChart(sensores){		
		
	$("#infochart").data("markerid","");
	$("#infochart").data("sensorid","");
	$("#infochart").data("deviceid","");		
	
	//console.log(sensores);
	//console.log(sensorvars);
	for(var i=0;i<sensorvars.length;i++){
		try{
			//console.log(sensores[sensorvars[i]]);
			if(sensorvars[i]){
				if(sensores[sensorvars[i]]){
					$("#"+sensorvars[i]+'div').data("sensorid",sensores[sensorvars[i]]);
					$("#"+sensorvars[i]+'div').data("sensor",sensorvars[i] );	
				}
				$("#"+sensorvars[i]+'div').click(
					function(){								
						var id="";
						try{
							id=$(this).attr("id");
						}catch(ex){}												
						var deviceid="";
						try{
							deviceid=$("#infochart").data("deviceid");
						}catch(ex){}
						
						var sensorid="";
						try{
							sensorid=$(this).data("sensorid");
						}catch(ex){}
						try{
							$("#infochart").data("sensorid",$(this).data("sensorid"));
						}catch(ex){}
						
						//console.log(sensorid);
						//console.log(deviceid);
												
						if(deviceid){
							sensordevice(sensorid,deviceid);	
						}					
					}				
				);					
			}		
			
				
		}catch(ex){	}		
		
	}			
}	
