module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        concat: {
            options: {
                banner: '<%= banner %>\n',
                stripBanners: true
            },
            smartsensor: {
                src: [
                    'js/app.js',
                    'js/services.js',
                    'js/models.js',
                    'js/controllers.js',
                    'js/filters.js',
                    'js/directives.js'
                ],
                dest: 'static/js/app.js'
            },
            vendors: {
                src: [
                    'bower_components/jquery/dist/jquery.min.js',
                    'bower_components/angular/angular.min.js',
                    'bower_components/angular-route/angular-route.min.js',
                    'bower_components/angular-resource/angular-resource.min.js',
                    'bower_components/angular-cookies/angular-cookies.min.js',
                    'bower_components/bootstrap/dist/js/bootstrap.js',
                    //'bower_components/mapbox.js/mapbox.js',
                    'bower_components/angular-mapbox/dist/angular-mapbox.js'
                ],
                dest: 'static/js/vendors.js'
            },
            cssvendors: {
                src: [
                    'bower_components/html5-boilerplate/css/normalize.css',
                    'bower_components/html5-boilerplate/css/main.css',
                  //  'bower_components/mapbox.js/mapbox.css'
                ],
                dest: 'static/css/vendors.css'
            }
        },

        uglify: {
            options: {
                preserveComments: 'some',
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            smartsensor: {
                src: '<%= concat.smartsensor.dest %>',
                dest: 'static/js/app.min.js'
            },
            vendors: {
                src: '<%= concat.vendors.dest %>',
                dest: 'static/js/vendors.min.js'
            }

        },
        less: {
            smartsensor: {
                options: {
                    strictMath: true,
                    sourceMap: true,
                    outputSourceFiles: true,
                    sourceMapURL: 'app.css.map',
                    sourceMapFilename: 'static/css/app.css.map'
                },
                files: {
                    'static/css/app.css': 'less/app.less'
                }
            }
        },

        cssmin: {
            options: {
                compatibility: 'ie8',
                keepSpecialComments: '*',
                noAdvanced: true
            },
            smartsensor: {
                files: {
                    'static/css/app.min.css': 'static/css/app.css'
                }
            },
            vendors: {
                files: {
                    'static/css/vendors.min.css': 'static/css/vendors.css'
                }
            }
        },

        usebanner: {
            options: {
                position: 'top',
                banner: '<%= banner %>'
            },
            files: {
                src: 'static/css/*.css'
            }
        },

        copy: {
            fonts: {
                expand: true,
                flatten: true,
                filter: 'isFile',
                cwd: 'bower_components/bootstrap/fonts/',
                src: '**',
                dest: 'static/fonts/'
            },
            modernizr: {
                src: 'bower_components/html5-boilerplate/js/vendor/modernizr-2.6.2.min.js',
                dest: 'static/js/modernizr.min.js'
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-banner');

    // Default tasks
    grunt.registerTask('make-js', ['concat', 'uglify']);
    grunt.registerTask('make-css', ['less', 'usebanner', 'cssmin']);

    grunt.registerTask('default', ['make-js', 'make-css', 'copy']);
};
