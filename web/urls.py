from django.conf.urls import patterns, url
from web import views

urlpatterns = patterns(
    'web.views',
    url(r'^$', views.index, name='index'),
    url(r'^button/$', views.button, name='button'),
)