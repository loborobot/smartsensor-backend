from django.shortcuts import render
import json
from django.conf import settings

def index(request):
    user = request.user
 # original
    response = render(request, 'index.html', {'debug': settings.DEBUG})

# en other source code
    #response = render(request, 'index.html')
    response.set_cookie('user', json.dumps({'username': user.username}), secure=None)
    return response

def button(request):
    response = render(request, 'button.html', {'debug': settings.DEBUG})
    return response
